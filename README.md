# Online Store Scrapper
A scrapper for some online mexican stores.

* Walmart - http://www.walmart.com.mx
* Superama - http://www.superama.com.mx
* Lacomer - http://www.lacomer.com.mx
* Soriana - http://www1.soriana.com

## Setup
Nothing special, just run following command in your terminal from project foler:
```
$ pip freeze < requirement.txt
$ python main.py
```
### /scrapers/modules
This folder contains different function for scraping for each store.  

#### /funcs.py
This file contains some basic function which use every of four scrappers.

* ##### ***format_title(title, shop)***
  Used for formatting title of departments, categories, products to correct form before inserting to DB. 
* ##### ***db_insert_product(db, product)***
  Takes **product data** and **db object** as a parameters to insert data to DB.
* ##### ***db_insert_department(db, name_value)***
  Same as above.
* ##### ***db_insert_category(db, dep, name_value)***
  Same as above.
* ##### ***db_insert_subcategory(db, cat, name_value)***
  Same as above.
* ##### ***db_insert_marca(db, name_value)***
  Same as above.

#### /walmart.py
  Contains some function for scraping data from **Walmart** and **Superama** shops.

* ##### ***get_departments(base_url)***
  **base_url** -- URL to shop API. It can be found in **manager.py** file at the root of the project.   
Used to get a JSON of departments.

* ##### ***def get_families(base_url, department)***
  Same as above.

* ##### ***get_lineas(base_url, department, family)***
  Same as above.

* ##### ***get_products(base_url, department, family, linea)***
  **department** -- department value to filter.    
**family** -- family(category) value to filter.    
**linea** -- linea(subcategory) value to filter.    
Used to get a JSON of available products by above filters.

* ##### ***get_product_information(product)***
  **product** -- **product** JSON as input.    
Used to get and sort data for further DB insertion.

#### /lacomer.py

* ##### ***get_menu_families(url)***
  **url** -- shop API url. Can be found in **manager.py**    
Used to get list of families(categories) values. 

* ##### ***get_products(page)***
  **page** -- parameter of page.    
Used to get a list of product from input page

* ##### ***get_page(section)***
  **section** -- section value parameter.    
Used to get number of pages for current section. 
 
* ##### ***get_product_information(product)***
  **product** -- product JSON as input.    
Used to parse product info for further inserting to DB.
  
#### /soriana.py

* ##### ***start_session()***
  Used to start session for collecting cookies.

* ##### ***get_sections(required)***
  **required** -- a list of required sections to scrape.    
Used to get a sections.

* ##### *** get_states(session)***
  **session** -- session object to retreive cookies.    
Used to get a list of states.

* ##### ***get_stores(session, href)***
 **href** -- state URL.    
Used to get a list of stores by state.

* ##### ***get_store_info(store)***
  **store** -- store info to parse.    
Used to parse a store parameter for further DB inserting.

* ##### ***get_products(section, page, session, p_code)***
  **section** -- section value    
**page** -- page value    
**session** -- session object    
**p_code** -- postal code    
Used to get products.

* ##### ***get_presentacion(title)***
  **title** -- presentaction value    
Used to format presentation value.

* ##### ***parse_product(product, marcas)***
  **product** -- product info to parse   
**marcas** -- list to choose marca for product from    
Used to parse product innfo for further insertion


### /scrapers
Every file contains scraping logic using scraping funcs described above.

### manager.py 
Flexible menu for choosing which scraper to run and use.