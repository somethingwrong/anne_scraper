CREATE TABLE IF NOT EXISTS soriana_departamento (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  titulo varchar(50) NOT NULL
);


CREATE TABLE IF NOT EXISTS soriana_categoria (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  titulo varchar(50) NOT NULL
);


CREATE TABLE IF NOT EXISTS soriana_subcategoria (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  titulo varchar(50) NOT NULL
);


CREATE TABLE IF NOT EXISTS soriana_estado (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  titulo varchar(50) NOT NULL
);


CREATE TABLE IF NOT EXISTS soriana_sucursal (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  titulo varchar(50) NOT NULL,
  direccion varchar(300) NOT NULL,
  ciudad varchar(100) NOT NULL,
  formato varchar(50) NOT NULL,
  estado tinyint(2) NOT NULL,
  FOREIGN KEY(estado) REFERENCES soriana_estado(id)
);


CREATE TABLE IF NOT EXISTS soriana_producto (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  nombre varchar(50) NOT NULL,
  presentacion varchar(20) NOT NULL,
  marca tinyint(3) NOT NULL,
  precio float(5,2) NOT NULL,
  fecha_actualizacion DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, -- ON UPDATE CURRENT_TIMESTAMP,
  uid varchar(50) NOT NULL UNIQUE,
  estado tinyint(2) NOT NULL,
  sucursal tinyint(3) NOT NULL,
  departamento tinyint(3) NOT NULL,
  categoria tinyint(3) NOT NULL,
  subcategoria tinyint(3),
  FOREIGN KEY(estado) REFERENCES soriana_estado(id),
  FOREIGN KEY(sucursal) REFERENCES soriana_sucursal(id),
  FOREIGN KEY(departamento) REFERENCES soriana_departamento(id),
  FOREIGN KEY(categoria) REFERENCES soriana_categoria(id),
  FOREIGN KEY(subcategoria) REFERENCES soriana_subcategoria(id)
);