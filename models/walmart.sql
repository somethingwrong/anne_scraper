-- phpMyAdmin SQL Dump
-- version 3.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 01, 2015 at 01:37 PM
-- Server version: 5.5.42-37.1
-- PHP Version: 5.4.43

-- SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
-- SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `cesarhr_walmart`
--

-- --------------------------------------------------------

--
-- Table structure for table `producto`
--

CREATE TABLE IF NOT EXISTS walmart_departamento (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  titulo varchar(50) NOT NULL
);


CREATE TABLE IF NOT EXISTS walmart_categoria (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  titulo varchar(50) NOT NULL
);


CREATE TABLE IF NOT EXISTS walmart_subcategoria (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  titulo varchar(50) NOT NULL
);


CREATE TABLE IF NOT EXISTS walmart_producto (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  nombre varchar(50) NOT NULL,
  presentacion varchar(20) NOT NULL,
  descripcion varchar(80) NOT NULL,
  marca tinyint(3) NOT NULL,
  precio float(5,2) NOT NULL,
  fecha_actualizacion DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, -- ON UPDATE CURRENT_TIMESTAMP,
  departamento tinyint(3) NOT NULL,
  categoria tinyint(3) NOT NULL,
  subcategoria tinyint(3) NOT NULL,
  uid int NOT NULL UNIQUE,
  FOREIGN KEY(departamento) REFERENCES walmart_departamento(id),
  FOREIGN KEY(categoria) REFERENCES walmart_categoria(id),
  FOREIGN KEY(subcategoria) REFERENCES walmart_subcategoria(id)
);

/* ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ; /;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
