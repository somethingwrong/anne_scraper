CREATE TABLE IF NOT EXISTS superma_departamento (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  titulo varchar(50) NOT NULL
);


CREATE TABLE IF NOT EXISTS superma_categoria (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  titulo varchar(50) NOT NULL
);


CREATE TABLE IF NOT EXISTS superma_subcategoria (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  titulo varchar(50) NOT NULL
);


CREATE TABLE IF NOT EXISTS superma_producto (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  nombre varchar(50) NOT NULL,
  presentacion varchar(20) NOT NULL,
  descripcion varchar(80) NOT NULL,
  marca tinyint(3) NOT NULL,
  precio float(5,2) NOT NULL,
  fecha_actualizacion DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, -- ON UPDATE CURRENT_TIMESTAMP,
  departamento tinyint(3) NOT NULL,
  categoria tinyint(3) NOT NULL,
  subcategoria tinyint(3) NOT NULL,
  uid int NOT NULL UNIQUE,
  FOREIGN KEY(departamento) REFERENCES superma_departamento(id),
  FOREIGN KEY(categoria) REFERENCES superma_categoria(id),
  FOREIGN KEY(subcategoria) REFERENCES superma_subcategoria(id)
);