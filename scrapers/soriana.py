# -*- coding: utf-8 -*-

import MySQLdb

from modules import soriana

o = [
    {'href': u'?p=11008',
     'categories':
         [
             {'href': u'?p=11009',
              'subcategories':
                  [
                      {'href': u'?p=11013',
                       'title': u'Aves'}],
              'title': u'Comidas Congeladas'}],
     'title': u'Alimentos Congelados y Refrigerados'}]


required_departments = [u'Abarrotes', u'Alimentos Congelados y Refrigerados', u'Carnes, Pescados y Mariscos',
                        u'Frutas y Verduras', u'Lácteos y Huevo', u'Pan, Tortillas y Alimentos Preparados',
                        u'Queso, Salchichonería y Gourmet', u'Vinos y Licores']

def scrap_products(esatdo_name, sucursal_name, department_name, category_name,
                   session, subcategory_title, href, db, p_code):
    for i in range(1, 400):
        products = soriana.get_products(href, i, session, p_code)
        if not products:
            break
        for product in products[0]:
            data = soriana.parse_product(product, products[1])
            data['departamento'] = department_name
            data['categoria'] = category_name
            data['subcategoria'] = subcategory_title
            data['estado'] = esatdo_name
            data['sucursal'] = sucursal_name
            soriana.db_insert_marca(db, data['marca'])
            soriana.db_insert_product(db, data)

def process(db_name, db_credentials):
    db = MySQLdb.connect(host=db_credentials['host'], user=db_credentials['user'], passwd=db_credentials['passwd'],
                         db=db_name)
    cursor = db.cursor()

    print 'Starting to fetch Soriana shop!'
    print 'Opened DataBase successfully!'

    session = soriana.start_session()
    print 'Session started!'
    states = soriana.get_states(session)
    for state in states:
        print '= Working with %s state!' % state['titulo']
        stores = soriana.get_stores(session, state['href'])
        soriana.db_insert_state(cursor, state['titulo'])
        db.commit()
        for store in stores:
            store_info = soriana.get_store_info(store)
            print "== Working with %s store!" % store_info['nombre']
            store_info['estado'] = state['titulo']
            soriana.db_insert_store(cursor, store_info)
            db.commit()
            session.get('http://www1.soriana.com/default.aspx' + store_info['href'])
            sections = soriana.get_sections(required_departments)
            for department in sections:
                soriana.db_insert_department(cursor, department['title'])
                db.commit()
                print "- Starting to fetch %s department!" % department['title']
                for category in department['categories']:
                    soriana.db_insert_category(cursor, department['title'], category['title'])
                    db.commit()
                    print "-- Starting to fetch %s category!" % category['title']
                    if len(category['subcategories']) > 0:
                        for subcategory in category['subcategories']:
                            soriana.db_insert_subcategory(cursor, category['title'], subcategory['title'])
                            db.commit()
                            print "--- Starting to fetch %s subcategory!" % subcategory['title']
                            scrap_products(state['titulo'], store_info['nombre'], department['title'],
                                           category['title'], session, subcategory['title'],
                                           subcategory['href'], cursor, store_info['pcode'])
                            db.commit()
                    else:
                        scrap_products(state['titulo'], store_info['nombre'], department['title'],
                                       category['title'], session, None, category['href'], cursor, store_info['pcode'])
                        db.commit()
    db.commit()
    db.close()
