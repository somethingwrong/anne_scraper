# -*- coding: utf-8 -*-

import MySQLdb

from modules import lacomer, funcs

base_url = 'http://www.lacomer.com.mx/lacomer/'



def scraper(db_credentials):
    db = MySQLdb.connect(host=db_credentials['host'], user=db_credentials['user'], passwd=db_credentials['passwd'],
                         db="cesarhr_lacomer")
    cursor = db.cursor()

    print 'Starting to fetch Lacomer shop!'
    print 'Opened DataBase successfully!'

    sections = lacomer.get_sections()
    for section in sections:
        print "Starting to fetch %s department, %s category!" % (section['departamento'], section['categoria'])
        pages = lacomer.get_page(section['id'])
        print pages
        for page in range(1, pages+1):
            print 'Fetching %s page!' % page
            products = lacomer.get_products(page)
            for product in products:
                pdata = lacomer.get_product_information(product)
                department_name = unicode(section['departamento'])
                category_name = unicode(section['categoria'])
                pdata['departamento'] = department_name
                pdata['categoria'] = category_name
                funcs.db_insert_department(cursor, department_name)
                funcs.db_insert_category(cursor, department_name, category_name)
                db.commit()
                funcs.db_insert_marca(cursor, pdata['marca'])
                funcs.db_insert_product(cursor, pdata)
                db.commit()