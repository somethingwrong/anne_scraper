# -*- coding: utf-8 -*-

import re
import requests

from bs4 import BeautifulSoup

base_url = 'http://www.lacomer.com.mx/lacomer/'

departments = [u'Abarrotes comestibles', u'Alimentos preparados', u'Dulces', u'Vinos, licores y cigarros',
               u'Bebidas', u'Frutas y verduras', u'Carne, pescado y salchichoneria', u'Lácteos y congelados', u'Raiz',
               u'Panadería y tortillería', u'Quesos']

def get_menu_families(url):
    html = requests.get(url=url).content
    soup = BeautifulSoup(html, 'html.parser')
    data = soup.find_all('a', class_='liga_a_suc')
    return data

def get_products(page):
    json_url = 'http://www.lacomer.com.mx/GSAServices/searchArt'
    params = {
        'col': 'lacomer_2',
        'orden': -1,
        'p': page,
        'passiloId': False,
        'succId': 14
    }
    response = requests.get(json_url, params=params).json()
    return response['res']

def parse_section_val(section):
    # Departamento  / Category / ID
    results = re.search(r'([\w\s\d\(\),]*) / ([\w\s\d,\(\)\.]*) / (\d+)', section, re.U)
    data = {
        'departamento': results.group(1),
        'categoria': results.group(2),
        'id': results.group(3)
    }
    return data

def get_sections():
    url = 'http://www.lacomer.com.mx/GSAServices/searchArt?succId=14'
    data = []
    response = requests.get(url=url).json()
    sections = response['din']['agru']
    for section in sections:
        section = parse_section_val(section['val'])
        if section['departamento'] in departments:
            data.append({'id': section['id'],
                         'departamento': section['departamento'],
                         'categoria': section['categoria']})
    return data

def get_page(section):
    url = 'http://www.lacomer.com.mx/GSAServices/searchArt'
    params = {
        'col': 'lacomer_2',
        'orden': -1,
        'pasilloId': section,
        'succId': 14
    }
    response = requests.get(url=url, params=params).json()
    return response['numpages']

def format_category(category):
    category = category.replace(' ', '+').lower()


def get_product_information(product):
    data = {}
    data['nombre'] = product['artDes'].replace("'", '`').upper()
    data['descripcion'] = product['artDes'].replace("'", '`')
    data['precio'] = product['artPrven']
    data['presentacion'] = str(product['artUco']) + ' ' + product['artTun']
    data['marca'] = product['marDes'].replace("'", '`')
    data['sid'] = str(product['agruId'])
    data['uid'] = int(product['artEan'])
    data['subcategoria'] = None
    return data