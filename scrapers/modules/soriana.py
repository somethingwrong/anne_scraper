# -*- coding: utf-8 -*-

import cookielib
import requests
import re

from bs4 import BeautifulSoup

import funcs

base_url = 'http://www1.soriana.com/site/default.aspx'


def start_session():
    s = requests.Session()
    s.get(url=base_url)
    return s


def get_sections(required):
    d_data = []
    url = base_url + '?p=1398'
    response = requests.get(url=url)
    html = response.content
    soup = BeautifulSoup(html, 'html.parser')
    department = soup.find('ul', id='MenuBar2').li
    departments = department.find_next_siblings()
    departments.append(department)
    for i in range(0, len(departments)):
        department_name = departments[i].a.text
        category = departments[i].ul.li
        categories = category.find_next_siblings()
        categories.append(category)
        d_data.append({
            'title': department_name,
            'href': departments[i].a['href'],
            'categories': []
        })
        for j in range(0, len(categories)):
            category_name = categories[j].a.text
            d_data[i]['categories'].append({
                'title': category_name,
                'href': categories[j].a['href'],
                'subcategories': []
            })
            try:
                subcategory = categories[j].ul.li
            except AttributeError:
                continue
            subcategories = subcategory.find_next_siblings()
            subcategories.append(subcategory)
            for g in range(0, len(subcategories)):
                subcategory_name = subcategories[g].a.text
                d_data[i]['categories'][j]['subcategories'].append({
                    'title': subcategory_name,
                    'href': subcategories[g].a['href']
                })
    d_data = [dep for dep in d_data if dep['title'] in required]
    return d_data


def get_states(session):
    url = 'http://www1.soriana.com/site/default.aspx?p=10129'
    response = session.get(url=url)
    html = response.content
    soup = BeautifulSoup(html, 'html.parser')
    states = soup.find_all('a', class_='txt_g_12pxubica')
    return [{'titulo': state.text, 'href': state['href']} for state in states]


def get_stores(session, href):
    url = base_url + href
    response = session.get(url=url)
    html = response.content
    soup = BeautifulSoup(html, 'html.parser')
    stores = soup.find_all('td', class_='marcoverde')[1:]
    return stores


def get_store_info(store):
    info = store.find_all('td')
    data = {
        'nombre': re.sub(r'\s{2,}', '', info[0].text),
        'direccion': re.sub(r'\s{2,}', ' ', info[1].text),
        'ciudad': re.sub(r'\s{2,}', '', info[2].text),
        'formato': info[3].text,
        'href': info[4].a['href']
    }
    data['pcode'] = re.search(r'CodigoPostal=(\d+)&', data['href']).group(1)
    ciudad_municipo = data['ciudad'].split(',')
    data['ciudad'] = ciudad_municipo[0]
    data['municipo'] = ciudad_municipo[1]
    data['callcenter'] = re.search(r'Call Center (.+)$', data['direccion']).group(1)
    data['telefono'] = re.search(r'Tel\. (.+)Call', data['direccion']).group(1)
    return data


def get_products(section, page, session, p_code):
    url = base_url + section + '&px=' + str(page)
    response = session.get(url=url)
    html = response.content
    max_pages = re.search(r'(\d+) páginas', html, re.U)
    max_pages = 1 if not max_pages else max_pages.group(1)
    if page > int(max_pages):
        return None
    soup = BeautifulSoup(html, 'html.parser')
    products = soup.find_all('div', class_='artDi4')
    brands = soup.find('select', {'name': 'select_marca'})
    brands = re.findall(r'>([\w\s]+)<', str(brands))
    uids = soup.find_all('div', class_='artDi3')
    uids = [re.search(r's=(\d+)', uid.a['href']).group(1) for uid in uids]
    products = [(products[i], p_code + '_' + uids[i]) for i in range(0, len(products))]
    return products, brands


def get_presentacion(title):
    title = re.sub(r'[()\.\*:]', '', title)
    title = re.sub(r'\s{2,}', ' ', title)
    title = re.sub(r'upc\s?\d+\s?\w\d+', '', title)
    try:
        title = title.encode('utf-8')
    except:
        title = title.decode('latin-1')
    pattern = r'([\d\.]{1,6}\s?\w{1,12}(\s\w{0,10})?)$'
    result = re.search(pattern, title, re.U)
    if result:
        return result.group(0)


def parse_product(product, marcas):
    data = {}
    data['nombre'] = product[0].find('div', class_='txtarticulohome').text.replace("'", "`")
    data['presentacion'] = get_presentacion(data['nombre'])
    precio = product[0].find('div', class_='precioarticulohome').text
    data['precio'] = re.search(r'\$([\d\.]+)', precio).group(1)
    data['uid'] = product[1]
    for marca in marcas:
        if marca.lower() in data['nombre'].lower():
            data['marca'] = marca
            break
        else:
            data['marca'] = ''
    return data


def db_insert_product(db, product):

    if product['subcategoria']:
        category_sql = ", (SELECT id FROM subcategoria WHERE titulo = '%s'))" % product['subcategoria']
        subcategory = ', subcategoria'
    else:
        category_sql = ")"
        subcategory = ''

    sql = "INSERT INTO producto (nombre, uid, presentacion," \
          "precio, marca, estado, sucursal, departamento, categoria%s) " \
          "VALUES ('%s', '%s', '%s', '%s', " \
          "(SELECT id FROM marca WHERE titulo = '%s'), " \
          "(SELECT id FROM estado WHERE titulo = '%s'), " \
          "(SELECT id FROM sucursal WHERE titulo = '%s'), " \
          "(SELECT id FROM departamento WHERE titulo = '%s'), " \
          "(SELECT id FROM categoria WHERE titulo = '%s')" \
          % (subcategory, product['nombre'], product['uid'], product['presentacion'],
             product['precio'], product['marca'], product['estado'], product['sucursal'],
             product['departamento'], product['categoria'])

    sql += category_sql
    sql += " ON DUPLICATE KEY UPDATE nombre=VALUES(nombre), presentacion=VALUES(presentacion), " \
           "precio=VALUES(precio)"
    db.execute(sql)



def db_insert_department(db, name_value):
    sql = "INSERT INTO departamento (titulo) " \
          "VALUES ('%s') " \
          "ON DUPLICATE KEY UPDATE titulo=VALUES(titulo)" % \
          name_value
    db.execute(sql)

def db_insert_category(db, dep, name_value):
    sql = "INSERT INTO categoria (titulo, id_departamento) " \
          "VALUES ('%s', (SELECT id FROM departamento WHERE titulo = '%s')) " \
          "ON DUPLICATE KEY UPDATE titulo=VALUES(titulo)" % (name_value, dep)
    db.execute(sql)

def db_insert_subcategory(db, cat, name_value):
    sql = "INSERT INTO subcategoria (titulo, id_categoria) " \
          "VALUES ('%s', (SELECT id FROM categoria WHERE titulo = '%s')) " \
          "ON DUPLICATE KEY UPDATE titulo=VALUES(titulo)" % (name_value, cat)
    db.execute(sql)

def db_insert_marca(db, name_value):
    sql = "INSERT INTO marca (titulo) VALUES ('%s') " \
          "ON DUPLICATE KEY UPDATE titulo=VALUES(titulo)" % name_value
    db.execute(sql)

def db_insert_state(db, state):
    sql = "INSERT INTO estado (titulo) " \
          "VALUES ('%s') " \
          "ON DUPLICATE KEY UPDATE titulo=VALUES(titulo)" % state
    db.execute(sql)


def db_insert_store(db, store):
    sql = "INSERT INTO sucursal (titulo, direccion, codigopostal, telefono, callcenter," \
          " ciudad, municipio, formato) " \
          "VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s') " \
          "ON DUPLICATE KEY UPDATE titulo=VALUES(titulo), direccion=VALUES(direccion)," \
          "codigopostal=VALUES(codigopostal), telefono=VALUES(telefono), callcenter=VALUES(callcenter), " \
          "ciudad=VALUES(ciudad), municipio=VALUES(municipio), formato=VALUES(formato)" % \
          (store['nombre'], store['direccion'], store['pcode'], store['telefono'],
           store['callcenter'], store['ciudad'], store['municipo'], store['formato'])
    db.execute(sql)
