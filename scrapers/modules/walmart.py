# -*- coding: utf-8 -*-

import requests
import re

from HTMLParser import HTMLParser

from . import funcs

def get_departments(base_url):
    url = base_url + 'hlMenuLeft.ashx'
    response = requests.get(url=url)
    return response.json()['MenuPrincipal'][0]['Elements']


def get_families(base_url, department):
    url = base_url + 'hlGetCategories.ashx'
    params = {
        'Departamento': department
    }
    response = requests.get(url=url, params=params)
    return response.json()


def get_lineas(base_url, department, family):
    url = base_url + 'hlGetCategories.ashx'
    params = {
        'Departamento': department,
        'Familia': family
    }
    response = requests.get(url=url, params=params)
    return response.json()


def get_products(base_url, department, family, linea):
    url = base_url + 'hlSearch.ashx'
    params = {
        'Departamento': department,
        'Familia': family,
        'Linea': linea
    }
    response = requests.get(url=url, params=params, headers={'content-type': 'json/application; charset=utf-8'})
    return response.json()['Products']


def get_product_information(product):
    data = {
        'nombre': HTMLParser().unescape(product['DescriptionDisplay']).replace("'", '`'),
        'presentacion': None,
        'descripcion': HTMLParser().unescape(product['DescriptionDisplay']).replace("'", '`'),
        'marca': HTMLParser().unescape(product['Brand']).replace("'", '`'),
        'precio': product['PrecioNumerico'],
        'uid': int(product['upc'])
    }
    data['nombre'].capitalize()
    return data


def get_presentacion(title):
    title = re.sub(r'[()\.\*:]', '', title)
    title = re.sub(r'\s{2,}', ' ', title)
    title = re.sub(r'upc\s?\d+\s?\w\d+', '', title)
    try:
        title = title.encode('utf-8')
    except:
        title = title.decode('latin-1')
    pattern = r'\d{0,5}\s[\d\w]{,12}(\sc\/u)?$'
    result = re.search(pattern, title, re.U)
    if result:
        return result.group(0)
