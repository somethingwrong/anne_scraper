# -*- coding: utf-8 -*-

import re


def format_title(title, shop):
    if shop != 'cesarhr_walmart':
        title = title[2:]
    title = re.sub('(-)|(_)', ' ', title)
    formatted_title = ''
    title = re.sub(r'\s\w$', '', title)
    for i in range(0, len(title)):
        if i == 0:
            formatted_title += title[i].upper()
        elif title[i-1] == ' ' and title[i+1] != ' ':
            formatted_title += title[i].upper()
        else:
            formatted_title += title[i]
    return formatted_title


def db_insert_product(db, product):

    if product['subcategoria']:
        category_sql = ", (SELECT id FROM subcategoria WHERE titulo = '%s'))" % product['subcategoria']
        subcategory = ', subcategoria'
    else:
        category_sql = ")"
        subcategory = ''

    sql = "INSERT INTO producto (nombre, uid, presentacion, descripcion," \
          "precio, marca, departamento, categoria%s) " \
          "VALUES ('%s', '%s', '%s', '%s', '%s', " \
          "(SELECT id FROM marca WHERE titulo = '%s')," \
          "(SELECT id FROM departamento WHERE titulo = '%s'), " \
          "(SELECT id FROM categoria WHERE titulo = '%s')" \
          % (subcategory, product['nombre'], product['uid'],
             product['presentacion'], product['descripcion'], product['precio'],
             product['marca'], product['departamento'], product['categoria'])

    sql += category_sql
    sql += " ON DUPLICATE KEY UPDATE nombre=VALUES(nombre), presentacion=VALUES(presentacion), " \
           "descripcion=VALUES(descripcion), precio=VALUES(precio)"

    db.execute(sql)

def db_insert_department(db, name_value):
    sql = "INSERT INTO departamento (titulo) " \
          "VALUES ('%s') " \
          "ON DUPLICATE KEY UPDATE titulo=VALUES(titulo)" % \
          name_value
    db.execute(sql)

def db_insert_category(db, dep, name_value):
    sql = "INSERT INTO categoria (titulo, id_departamento) " \
          "VALUES ('%s', (SELECT id FROM departamento WHERE titulo = '%s')) " \
          "ON DUPLICATE KEY UPDATE titulo=VALUES(titulo)" % (name_value, dep)
    db.execute(sql)

def db_insert_subcategory(db, cat, name_value):
    sql = "INSERT INTO subcategoria (titulo, id_categoria) " \
          "VALUES ('%s', (SELECT id FROM categoria WHERE titulo = '%s')) " \
          "ON DUPLICATE KEY UPDATE titulo=VALUES(titulo)" % (name_value, cat)
    db.execute(sql)

def db_insert_marca(db, name_value):
    sql = "INSERT INTO marca (titulo) VALUES ('%s') " \
          "ON DUPLICATE KEY UPDATE titulo=VALUES(titulo)" % name_value
    db.execute(sql)
