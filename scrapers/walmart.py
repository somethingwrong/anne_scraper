# -*- coding: utf-8 -*-

import MySQLdb
import re

from modules import walmart, funcs

def process(base_url, db_name, cols, db_credentials):
    db = MySQLdb.connect(host=db_credentials['host'], user=db_credentials['user'], passwd=db_credentials['passwd'],
                         db=db_name)
    cursor = db.cursor()

    if db_name == 'cesarhr_walmart':
        sec_pref = [u'd-', u'f-', u'l-']
    else:
        sec_pref = [u'', u'', u'']

    print "Starting to fetch %s shop!" % db_name
    print "Opened database successfully"
    departments = walmart.get_departments(base_url)
    for department in departments[cols[0]:cols[1]]:
        department_name = funcs.format_title(department['departmentName'], db_name)
        funcs.db_insert_department(cursor, department_name)
        db.commit()
        print "- Starting to fetch %s department!" % department_name
        families = walmart.get_families(base_url, sec_pref[0] + department['departmentName'])
        for family in families:
            family_name = funcs.format_title(family['ID'], db_name)
            lineas = walmart.get_lineas(base_url, sec_pref[0] + department['departmentName'], sec_pref[1] + family['ID'])
            funcs.db_insert_category(cursor, department_name, family_name)
            db.commit()
            print "-- Starting to fetch %s category!" % family_name
            for linea in lineas:
                linea_name = funcs.format_title(linea['ID'], db_name)
                products = walmart.get_products(base_url, sec_pref[0] + department['departmentName'],
                                                sec_pref[1] + family['ID'], sec_pref[2] + linea['ID'])
                funcs.db_insert_subcategory(cursor, family_name, linea_name)
                db.commit()
                print "--- Starting to fetch %s subcategory!" % linea_name
                for product in products:
                    data = walmart.get_product_information(product)
                    data['departamento'] = department_name
                    data['categoria'] = family_name
                    data['subcategoria'] = linea_name
                    data['presentacion'] = walmart.get_presentacion(data['nombre'])
                    if data['presentacion']:
                        data['nombre'] = re.sub(data['presentacion'], '', data['nombre'])
                        data['nombre'] = re.sub(r'por$', '', data['nombre'])
                    funcs.db_insert_marca(cursor, data['marca'])
                    db.commit()
                    try:
                        funcs.db_insert_product(cursor, data)
                    except MySQLdb.OperationalError as e:
                        print data['marca']
                        print e
                    db.commit()

    db.commit()
    print 'Changes commited!'
    db.close()
    print 'Database has been closed successfully!'
