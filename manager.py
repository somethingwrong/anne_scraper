import os
import threading
import json

from scrapers import walmart
from scrapers import lacomer
from scrapers import soriana

def run_process():
    with open('db.json') as json_data:
        db_credentials = json.load(json_data)
    superma_url = 'http://www.superama.com.mx/superama/WebControls/'
    walmart_url = 'https://www.walmart.com.mx/super/WebControls/'
    while True:
        print "Please, make your choice. (number)\n" \
              "1. Walmart\n" \
              "2. Superma\n" \
              "3. Lacomer\n" \
              "4. Soriana\n" \
              "5. Time settings\n" \
              "6. Exit"
        choice = raw_input(">> ")
        if choice == '1':
            walmart.process(walmart_url, 'cesarhr_walmart', (0, 9), db_credentials)
        elif choice == '2':
            walmart.process(superma_url, 'cesarhr_superama', (1, 10), db_credentials)
        elif choice == '3':
            lacomer.scraper(db_credentials)
        elif choice == '4':
            soriana.process('cesarhr_soriana', db_credentials)
        elif choice == '5':
            print "Please, type the daytime in such form: hh:mm (0-23:0-59)"
        elif choice == '6':
            print 'Thank you, goodbye!'
            exit()
        else:
            print 'Wrong input! tip: use number for decision'

run_process()